# Release Process

This project defines multiple templates to be used in gitlab-ci pipelines.

There are multiple pipeline templates in the root of the project which are a bit opinionated. Those pipelines are
composed of job templates which are designed to be as 'composable' as possible for maximum reuseability.

You can use one of the pipeline templates or compose your own pipelines using the job templates.

For a definition of pipeline templates and job templates see the [template types] documentation of Gitlab. More information
about job templates can be found in the [Job templates README]

[template types]: https://docs.gitlab.com/ee/development/cicd/templates.html#template-types
[Job templates README]: jobs/README.md

## Goals

For **job templates** the goals are:

* maximize reusability and customizability
* provide proven/tested building blocks for the pipline templates and for custom pipelines

For **pipeline templates** the goals are:

* provide a easy to use pipeline for common use cases
* run fast: run as much in parallel as possible
* fail fast: run tests as soon as possible
* encourage best practices by assuming specific file locations/file names
* ease of use is more important than customizability as the user can always build a custom pipeline from the job
  templates
* automatically enabled/disable components of the pipeline if reasonable (e.g. no tests/webpage present)

## Pipeline templates

Pipeline templates define a opionated workflow for different usecases. They are composed of job templates and can be
extended by additional jobs. Pipeline templates are assuming that specific `stages` are present and provide their own
`stages` block if needed (currently all templates use the default stages of gitlab however).

### Generic (`generic.yml`)

```mermaid
flowchart LR
    build --> test --> deploy

    subgraph build
        direction LR
        merge-request
    end
    subgraph test
        direction LR
    end
    subgraph deploy
        direction LR
        pages
        semantic-release
    end
```

#### Job `merge-request`

This job automatically creates a merge request if the branch doesn't have one already.

| Branch                  | Target branch                                                         | Squash  |
|-------------------------|-----------------------------------------------------------------------|---------|
| default branch          | *no merge request is created*                                         | -       |
| `$INTEGRATION_BRANCH`   | default branch                                                        | `false` | 
| `feat/*` or `feature/*` | `$INTEGRATION_BRANCH` (or default branch if this variable is not set) | `true`  |

#### Job `pages`

Generate a static website with [Retype] if a `retype.yml` is present at the root of the project. This webpage is
automatically published to gitlab pages for the default branch.

#### Job `semantic-release`

Run [semantic-release] if a `.releaserc`, `.releaserc.yml`, `.releaserc.yaml`, `.releaserc.json`, or `releaserc.json`
is present. It installs dependencies from a `package.json` automatically if this file is present at the project root.

### Container Image (`containerimage.yml`)

A pipeline for `Dockerfile` based container images. The images are built with `docker buildx build` and can be tested
with [container-structure-test].

```mermaid
flowchart LR
    build --> test --> deploy

    subgraph build
        direction LR
        merge-request
        docker-build
    end
    subgraph test
        direction LR
        hadolint
        container-structure-test
    end
    subgraph deploy
        direction LR
        docker-promote-latest
        docker-publish
        pages
        semantic-release
    end
```

Note: This pipeline extends the `generic.yml` pipeline - only the additional jobs are described here.

#### Job `hadolint`

Runs [hadolint] on a `Dockerfile` at project root if it is present.

#### Job `docker-build`

Runs `docker buildx build` on a `Dockerfile` at project root if it is present. The image will be automatically published
to the gitlab container registry with the tags `build-$CI_PIPLINE_IID` and `$CI_COMMIT_REF_SLUG`.

You can build a multi-arch container image by passing a space or comma separated list of `os/arch` in the `PLATFORMS`
variable (e.g. `linux/amd64 linux/arm64`). By default only `linux/amd64` images are built.

#### Job `container-structure-test`

Runs [container-structure-test] if a `container-structure-test.yml` file is present. Only the `linux/amd64` platform is
tested by default. You can use matrix builds to test multiple platforms at the same time. Make sure that you actually
build those with the `docker-build` step before.

```yaml
container-structure-test:
  parallel:
    matrix:
      - PLATFORM:
          - linux/amd64
          - linux/arm64
```

#### Job `docker-promote-latest`

Updates the `latest` tag in the gitlab internal registry to the image built in `docker-build`.

#### Job `docker-publish`

If a `$PUBLISH_REGISTRY_IMAGE` is set, the image is copied to this location using `$PUBLISH_REGISTRY_USER` and
`$PUBLISH_REGISTRY_PASSWORD` as credentials. This job is disabled if `$PUBLISH_REGISTRY_IMAGE` is not set.

### Go binary (`go-binary.yml`)

A pipeline for go binaries for multiple architecture.

```mermaid
flowchart LR
    build --> test --> deploy

    subgraph build
        direction LR
        merge-request
        go-build
    end
    subgraph test
        direction LR
        golangci-lint
        go-test
    end
    subgraph deploy
        direction LR
        pages
        semantic-release
    end
```

Note: This pipeline extends the `generic.yml` pipeline - only the additional jobs are described here.

#### Job `go-build`

Runs `go build` on the root of the project. Binaries for multiple platforms (`linux/amd64`, `linux/arm64`
, `darwin/amd64`, `darwin/arm64`, `windows/amd64`) are generated and published as an artifact. You can overwrite the
list of target platforms by passing a `PLATFORMS` variable with space separated `os/arch` combinations to the job.

#### Job `golangci-lint`

Runs [golangci-lint] against all go sources.

#### Job `go-test`

Runs `go test` in the root of the repository if at least one `*_test.go` file is present in the repository.

[Retype]: https://retype.com/

[semantic-release]: https://semantic-release.gitbook.io/

[container-structure-test]: https://github.com/GoogleContainerTools/container-structure-test

[hadolint]: https://github.com/hadolint/hadolint

[golangci-lint]: https://golangci-lint.run/
