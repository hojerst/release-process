# Jobs

All templates in this folder are providing building blocks for actual pipelines. This means every job defined in the
templates are intended to be used in a `extends` block of an actual job definition. This means that all jobs defined
here MUST start with a dot (`.`) so they are not executed right ahead when included.

Corresponding jobs are located in the same folder, with one job per file. The filename and folder name is represented in
the actual job template name. e.g. for `containerimage/docker-build.yml` the corresponding job definition should be
named `.containerimage:docker-build`.

As the job templates in this folders are meant to be reused, they should NOT assume the presence of specific
stages/jobs. It is the responsibility of the user to create real jobs and order them accordingly.

To enable reuse of job templates, only a subset of the possible gitlab keywords are supported:

- `artifacts`
- `cache`
- `coverage`
- `image`
- `interruptible`
- `script`
- `services`
- `variables`

Note: The use of `before_script` and `after_script` is forbidden, so users can easily extend jobs with their own logic.
Please add your validation directly to the `script` section of the job definition.
